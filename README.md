# Front End Boilerplate
Pastikan sudah install [Nodejs](https://nodejs.org/en/), [Git](https://git-scm.com/), dan [Gulpjs](http://gulpjs.com/).

## Initial Setup
```sh
$ git clone git@gitlab.com:salt-frontend/def-html.git
$ cd def-html/Assets/core
$ npm install
```

## Running Development
```sh
$ cd def-html/Assets/core
$ gulp
```



## Directory Structure
```
- Assets
	- core (front end ubah/tambah file css/js/html di folder ini)
		- scss
		- script
		- views
	- css
	- js
	- images
- Public (hasil generate html ada di folder ini)
```