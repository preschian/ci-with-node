var gulp            = require('gulp')
var gutil           = require('gulp-util')
var sass            = require('gulp-sass')
var postcss         = require('gulp-postcss')
var sourcemaps      = require('gulp-sourcemaps')
var autoprefixer    = require('autoprefixer')
var uglifycss       = require('gulp-uglifycss')
var uglifyjs        = require('gulp-uglifyjs')
var concat          = require('gulp-concat')
var browserSync     = require('browser-sync').create()
var path            = require('path')
var html            = require('gulp-processhtml')
var watch           = require('gulp-watch')
var runs            = require('run-sequence')
var clean           = require('gulp-clean')

var file = {
    html: 'views/pages/*.html',
    sass: {
        main: 'sass/*.scss',
        pages: 'sass/pages/*.scss',
        vendor: [
            'bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/slick-carousel/slick/slick.css',
            'bower_components/slick-carousel/slick/slick-theme.css',
            'bower_components/font-awesome/css/font-awesome.css',
            'bower_components/animate_css/animate.css',
            'bower_components/Dropzone/dropzone.scss',
            'bower_components/custom-scrollbar/jquery.mCustomScrollbar.css',
            'plugins/**/*.css'
        ]
    },
    js: {
        main: 'script/*.js',
        pages: 'script/pages/*.js',
        vendor: [
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/slick-carousel/slick/slick.min.js',
            'bower_components/Dropzone/dropzone.js',
            'bower_components/custom-scrollbar/jquery.mCustomScrollbar.js',
            'plugins/**/*.js'
        ]
    }
}

// html task
gulp.task('html:clean', function() {
    return gulp.src('../../public/', {read: false})
        .pipe(clean({force: true}));
})

gulp.task('html:main', function() {
    return gulp.src(file.html)
        .pipe(html())
        .pipe(gulp.dest('../../public'))
})

gulp.task('html', function() {
    runs('html:clean', 'html:main')
})

// sass task
gulp.task('sass:clean', function() {
    return gulp.src([
        '../css/*.css',
        '../css/maps/',
        '../css/pages/'
        ], {read: false})
        .pipe(clean({force: true}));
})

gulp.task('sass:main', function() {
    return gulp.src(file.sass.main)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: 'node_modules/'
        }).on('error', sass.logError))
        .pipe(postcss([autoprefixer({ browsers: ['> 0%'] })]))
        .pipe(sourcemaps.write('../css/maps', {
            includeContent: false,
            sourceMappingURL: function(file) {
                return 'maps/' + file.relative + '.map'
            }
        }))
        .pipe(gulp.dest('../css'))
        .pipe(browserSync.stream())
})

gulp.task('sass:pages', function() {
    return gulp.src(file.sass.pages)
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: 'node_modules/'
        }).on('error', sass.logError))
        .pipe(postcss([autoprefixer({ browsers: ['> 0%'] })]))
        .pipe(sourcemaps.write('../maps/pages', {
            includeContent: false,
            sourceMappingURL: function(file) {
                return '../maps/pages/' + file.relative + '.map'
            }
        }))
        .pipe(gulp.dest('../css/pages'))
        .pipe(browserSync.stream())
})

gulp.task('sass:vendor', function() {
    return gulp.src(file.sass.vendor)
        .pipe(concat('vendor.css'))
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: 'node_modules/'
        }).on('error', sass.logError))
        .pipe(gulp.dest('../css'))
        .pipe(browserSync.stream())
})

gulp.task('sass', function() {
    runs('sass:clean', 'sass:main', 'sass:pages', 'sass:vendor')
})

// js task
gulp.task('js:clean', function() {
    return gulp.src('../js/', {read: false})
        .pipe(clean({force: true}));
})

gulp.task('js:main', function() {
    return gulp.src(file.js.main)
        .pipe(concat('app.js'))
        .pipe(uglifyjs()).on('error', function() {
            this.emit('end')
        })
        .pipe(gulp.dest(path.join(__dirname, '../js')))
})

gulp.task('js:pages', function() {
    return gulp.src(file.js.pages)
        .pipe(uglifyjs())
        .pipe(gulp.dest(path.join(__dirname, '../js/pages')))
})

gulp.task('js:vendor', function() {
    return gulp.src(file.js.vendor)
        .pipe(concat('vendor.js'))
        .pipe(uglifyjs())
        .pipe(gulp.dest(path.join(__dirname, '../js')));
})

gulp.task('js', function() {
    runs('js:clean', 'js:main', 'js:pages', 'js:vendor')
})

// watch task
gulp.task('watch', function() {
    // watch sass
    watch('sass/**/*.scss', function() {
        runs('sass:main')
    })

    // watch javascript
    watch('script/**/*.js', function() {
        runs('js:main', function() {
            browserSync.reload()
        })
    })

    // watch html
    watch('views/**/*.html', function() {
        runs('html:main', function() {
            browserSync.reload()
        })
    })
})

// serve browserSync
gulp.task('serve', function() {
    var serveOption = {
        server: {
            baseDir: [
                '../../public',
                '../../'
            ]
        }
    }

    browserSync.init(serveOption)
})

// default task, just run:
// $ gulp
gulp.task('default', function() {
    runs('html', 'sass', 'js', 'watch', 'serve')
})