

// IF DROPZONE IS EXIST
if("function" == typeof Dropzone) {

	function onDelete(e) {
	    var e = $(e),
	        t = e.parents(".fileinput").find(".editerror");
	    console.log(t), t.val("false")
	}

	function setDropzoneOption() {
	    var e = '<div style="display: none;"><div id="template" class="file-row"><div class="row"><div class="col-xs-12"><p class="name" data-dz-name></p><strong class="error text-danger" data-dz-errormessage></strong></div></div><div class="row"><div class="col-xs-11"><div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"> <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div></div></div><div class="col-xs-1"> <button data-dz-remove onclick="onDelete(this)" class="delete"><span>X</span> </button></div></div></div></div>';
	    $("body").append(e);
	    var t = document.querySelector("#template");
	    t.id = "", previewTemplate = t.parentNode.innerHTML, t.parentNode.removeChild(t)
	}

	// call this function to use dropzone
	// initDropzone(target, URL, TYPE)
	// target : Targeted element by ID - String
	// URL : Url to upload process - String
	// TYPE : Excepted file type. Ex. 'jpeg, png, zip' - String
	// Usage : initDropzone("upload-1", "/Project/SaveUploadedFile", ".jpeg, .jpg");
	function initDropzone(el, url, type) {
	    var n = url, e = el, i = type
	        o = new Dropzone(document.getElementById(e), {
	            url: n,
	            thumbnailWidth: 80,
	            thumbnailHeight: 80,
	            parallelUploads: 20,
	            previewTemplate: previewTemplate,
	            acceptedFiles: i,
	            clickable: "#" + e + " .fileinput-button"
	        });
	    o.on("addedfile", function(e) {}), o.on("totaluploadprogress", function(e) {}), o.on("sending", function(e) {}), o.on("success", function(e, t) {
	        e.previewElement.parentElement.querySelector(".form-control.filename").value = e.name, e.previewElement.parentElement.querySelector(".editstatus") && (e.previewElement.parentElement.querySelector(".editstatus").value = !0), e.previewElement.parentElement.querySelector(".editerror") && (e.previewElement.parentElement.querySelector(".editerror").value = !1)
	    }), o.on("error", function(e, t) {
	        "error" == e.status && e.previewElement.parentElement.querySelector(".editerror") && (e.previewElement.parentElement.querySelector(".editerror").value = !0)
	    }), $(document).on("click", ".fileinput button.delete", function() {
	        var e = $(this).parents(".fileinput").find(".editerror");
	        console.log(e), e.val("false")
	    })
	}

}


(function($){

 	if($('[placeholder]').length > 0){
        $('input, textarea').placeholder();
    }

    // INIT SCROLLBAR
    //if("function" == typeof mCustomScrollbar) {
	    $("body").mCustomScrollbar({ theme:"dark-thin", scrollbarPosition: "inside" });
	    if($(".scrollbar").length > 0) {
	    	$(".scrollbar").mCustomScrollbar({ theme:"dark-thin", scrollbarPosition: "inside" });
	    }
	//}

    if("function" == typeof Dropzone) {

		var previewTemplate = "";
		setDropzoneOption();
		initDropzone("upload-1", "/Project/SaveUploadedFile", ".jpeg, .jpg");

    }


})(jQuery);